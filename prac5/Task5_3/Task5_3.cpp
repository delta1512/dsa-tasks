#include <iostream>
#include <stack>
#include <vector>

using namespace std;

#include "state.h"

bool search(stack<State> states, State target) {
	if (states.empty()) {
		return false;
	}

	if (!(target == states.top())) {
		states.pop();
		return search(states, target);
	} else {
		return true;
	}
}


int main() {
	srand(time(0));

	State target;
	target.printBoard();
//	states.push(target);

	stack<State> states;
	for (int i = 0; i < 500; i++) {
		State s;
		states.push(s);
	}

	if (search(states, target))
		cout << "Found it! " << endl;
	else
		cout << "Did not found it!" << endl;

	return 0;
}
