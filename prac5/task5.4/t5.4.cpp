#include <iostream>
#include <cstdlib>
#include <ctime>

#include "state.h"


int main() {
  srand(time(NULL));
  int src, dest;
  int boardSize = 3;
  int nums = 6;
  State myState = State(3, 6);

  // Show what the board looks like
  cout << "This is the current board:" << endl;
  myState.showBoard();

  cout << "Choose a column to pull a tile from: ";
  cin >> src;

  cout << "Now choose a column to deposit the tile: ";
  cin >> dest;

  // 1 means success, 0 means failure
  bool result = myState.moveBlock(src, dest);

  cout << endl << "The result is " << result << " and here is the new board: " << endl;
  myState.showBoard();

  return 0;
}
