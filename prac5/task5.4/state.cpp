#include <iostream>
#include <cstdlib>
#include <string>
#include <assert.h>

#include "state.h"


// Random function used in my final assignment
int getRand(int a, int b) {
  return rand() % (b-a+1) + a;
}


// Starts a state with a completely random board
// s is the size of the board (eg s=3, it will create a 3x3 board)
// n is the amount of numbers to place on the board
State::State(int s, int n) {
  size = s;
  nums = n;
  assert(n < size*size);
  initBoard();
  randomiseBoard();
}


State::State(int s, int n, int* boardDef) {
  size = s;
  nums = n;
  assert(n < size*size);
  initBoard();

  for (int x = 0; x < size; x++) {
    for (int y = 0; y < size; y++) {
      board[x][y] = boardDef[(x*size) + y];
    }
  }
}


// Zeroes-out the board and initialises memory, ready for game setup
void State::initBoard() {
  board = new int*[size];
  for (int x = 0; x < size; x++) {
    board[x] = new int[size];
    for (int y = 0; y < size; y++) {
      board[x][y] = 0;
    }
  }
}


// Zeroes-out the board
void State::clearBoard() {
  for (int x = 0; x < size; x++) {
    for (int y = 0; y < size; y++) {
      board[x][y] = 0;
    }
  }
}


void State::randomiseBoard() {
  // Count iterations over the columns
  int iterations, currentCol=0;
  for (int i = 1; i <= nums; i++) {
    // Get a random number of iterations (the larger the number, the more random it is)
    iterations = getRand(10, 10*size);
    // Keep iterating for `iterations` and until you come across a free space
    while (iterations > 0 || top(currentCol) != 0) {
      // Decrement only on free space
      if (top(currentCol) == 0) {
        iterations--;
      }
      currentCol++;
      currentCol = currentCol % size;
    }
    // Put the number in that column
    insertBlockTo(currentCol, i);
  }
}


// Gets the item at the top of a column (the very top, not the top of the stack)
// Zero means that it has space
int State::top(int col) {
  //assert(col < size && col >= 0);
  // Recreate the assertion above as an if-statement
  if (!(col < size && col >= 0)) {
    return -1;
  }
  return board[size-1][col];
}


bool State::insertBlockTo(int col, int val) {
  //assert(top(col) == 0 && val > 0 && val <= nums);
  // Recreate the assertion above to handle erroneous data
  if (!(top(col) == 0 && val > 0 && val <= nums)) {
    return false;
  }

  int i = 0;
  int current = board[0][col];
  while (current != 0 && i < size-1) {
    i++;
    current = board[i][col];
  }

  if (current == 0) {
    board[i][col] = val;
  }
  return true;
}


int State::removeBlockFrom(int col) {
  int i = size-1;
  int current = board[i][col];
  while (current == 0 && i > 0) {
    i--;
    current = board[i][col];
  }

  if (current != 0) {
    int temp = board[i][col];
    board[i][col] = 0;
    return temp;
  } else {
    return -1;
  }
}


bool State::moveBlock(int src, int dest) {
  int tile = removeBlockFrom(src);
  if (tile == -1) {
    return false;
  }

  // If a failure occurred, replace the tile
  if (!insertBlockTo(dest, tile)) {
    insertBlockTo(src, tile);
    return false;
  }

  return true;
}


void State::showBoard() {
  // Push to a new line
  cout << endl;
  // Re-usable separators
  string preRowSep = "";
  for (int i = 0; i < size; i++) {
    preRowSep += string("|   ");
  }
  preRowSep += "|\n";

  string rowSep = "+";
  for (int i = 0; i < size; i++) {
    rowSep += string("---+");
  }
  rowSep += "\n";


  // Start from top to bottom (x is rows)
  for (int x = size-1; x >= 0; x--) {
    cout << preRowSep;
    for (int y = 0; y < size; y++) {
      cout << "| " << board[x][y] << " ";
    }
    // Fix off-by-one bug
    cout << "|" << endl;
    cout << preRowSep;
    cout << rowSep;
  }
}


State::~State() {
  for (int x = 0; x < size; x++) {
    delete[] board[x];
  }

  delete[] board;
}
