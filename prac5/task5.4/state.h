#ifndef state_H
#define state_H

using namespace std;


class State {
  int** board;
  int size;
  int nums;

  int top(int col);
  void initBoard();
  void clearBoard();
  void randomiseBoard();

  // Functions for t5.4
  bool insertBlockTo(int col, int val);
  int removeBlockFrom(int col);
  //

  public:
    State(int s, int n);
    State(int s, int n, int* boardDef);
    void showBoard();

    // Function for t5.4
    bool moveBlock(int src, int dest);
    //

    ~State();
};


#endif
