#include <iostream>


using namespace std;


double power(int x, int y);


int main() {
  cout << "Powers of 2: " << endl;
  for (int i = 0; i <= 16; i++) {
    cout << "2^" << i << ": " << power(2, i) << endl;
  }

  cout << "Inverse powers of 2: " << endl;
  for (int i = 0; i <= 16; i++) {
    cout << "2^" << -i << ": " << power(2, -i) << endl;
  }

  return 0;
}


double power(int x, int y) {
  if (y < 0) {
    return 1/power(x, -y);
  } else if (y > 1) {
    return (double) x * power(x, y-1);
  } else if (y == 1) {
    return (double) x;
  } else {
    return 1;
  }
}
