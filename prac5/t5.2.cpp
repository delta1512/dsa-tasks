#include <iostream>

using namespace std;


void printStars(int n);
void printLines(int n);


int main() {
  printLines(4);
  return 0;
}


void printStars(int n) {
  if (n > 1) {
    printStars(n-1);
    cout << "*";
  } else {
    cout << "*";
  }
}


void printLines(int n) {
  if (n >= 1) {
    printStars(n);
    cout << endl;
    printLines(n-1);
    printStars(n);
    cout << endl;
  }
}
