#include <iostream>
#include <fstream>

using namespace std;

string fileToStr(string fname);


int main() {
  cout << fileToStr("article.txt") << endl;
}


string fileToStr(string fname) {
  ifstream input;
  string final;
  input.open(fname);
  if (!input.good()) {
    cout << "Failed to open file: " << fname << endl;
    return "";
  }

  string line;
  while (getline(input, line)) {
    final = final + line + "\n";
  }
  input.close();

  return final;
}
