#include<iostream>
#include<stdlib.h>
#include<iomanip>
#include<string>

using namespace std;

#include "AVL_ADT.h"
#include "AVL_ADT_Ext.h"

struct Data { // as an example

	string key;
	int info;
};

void print(Data ss) { // as an example
	cout << ss.key <<", "<< ss.info << endl;
}

int main() {
	// for testing only
	AVLTreeExt<Data, string> tree;

	Data newItem;
	newItem.key = "Dongmo";
	newItem.info = 10;
	tree.AVL_Insert(newItem);

	newItem.key = "John";
	newItem.info = 20;
	tree.AVL_Insert(newItem);

	cout<<"\nPrint the tree:" <<endl;
	tree.AVL_Print();

	cout<<"\n In-order traversal of the data in the tree:" <<endl;
	tree.AVL_Traverse(print);

    return 0;
}
