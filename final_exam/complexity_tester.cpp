#include <iostream>

using namespace std;

int nSquared(int n);
int logN(int n);
int nLogN(int n);


int main() {
  int (*f)(int n) = nLogN;
  for (int i = 2; i <= 100; i+=10) {
    cout << "n=" << i << " ---> " << f(i) << endl;
  }

  return 0;
}


int nSquared(int n) {
  int s = 0;
  for (int i = 0; i < n; i++) {
    for (int j = 0; j < n; j++) {
      s++;
    }
  }

  return s;
}


int logN(int n) {
  int s = 0;
  for (int i = 0; i < n; i++) {
    s++;
    i *= 2;
  }

  return s;
}


int nLogN(int n) {
  int s = 0;
  for (int i = 0; i < n; i++) {
    for (int j = 0; j < n; j++) {
      s++;
      j *= 2;
    }
  }

  return s;
}
