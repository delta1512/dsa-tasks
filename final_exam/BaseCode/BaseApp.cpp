/*
 * The following code is provided for you to complete the programming tasks for 300103 DSA final examination
 * You may use, add, modify or delete any part of the code. Some parts of the code are hints.
 * You may ignore or replace any of them with your own implementation
 */

#include<iostream>
#include<fstream>
#include<cstdlib>
#include<iomanip>
#include<ctime>
#include <string>
#include <set>
#include <vector>
#include <queue>
#include <unordered_set>

using namespace std;

#include "AVL_ADT.h"
#include "AVL_ADT_Ext.h"

//Provided code. You may use, modify or delete it
struct Name {
	string first_name;
	string last_name;

	bool operator<(const Name& d) const {
		return last_name > d.last_name;
	}

    void display() {
	cout << first_name << " " << last_name << endl;
    }
};

//Provided code. You may use, modify or delete it
struct Data {
	int key;
	Name name;

	bool operator<(const Data& d) const {
		return name < d.name;
	}
};

//Provided code. You may use, modify or delete it
void print(Data ss) {
	cout << ss.key << ": " << ss.name.first_name << " " << ss.name.last_name
			<< endl;
}

//Provided code. You may use, modify or delete it
vector<Name> inputNames() { //This function is to read a list of names from the provided text file and store it to a vector
	ifstream fin("names.txt");
	string word;
	vector<Name> namelist;

	while (!fin.eof()) {
		Name name;
		fin >> name.first_name;
		fin >> name.last_name;
		namelist.push_back(name);
	}

	return namelist;
}


// TASK 1
// Get a random number between two positive integers a and b (inclusive)
int getRand(int a, int b) {
  return rand() % (b-a+1) + a;
}

vector<int> getTenRandNums() {
	vector<int> final;
	// Use of an unordered set for unique numbers
	unordered_set<int> uniqueRand;
	while (uniqueRand.size() < 10) {
		uniqueRand.insert(getRand(100, 200));
	}

	unordered_set<int>::iterator i = uniqueRand.begin();
	for (int x = 0; x < 10; x++) {
		final.push_back(*i);
		i++; // Move to next element in the unordered_set
	}

	return final;
}


int main() {
	srand(time(0));
	AVLTreeExt<Data, int> tree;
	// TASK 1
	vector<int> randNums = getTenRandNums();

	cout << "TASK 1: " << endl;
	for (int i = 0; i < 10; i++) {
		cout << randNums[i] << " ";
	}
	cout << endl << endl;

	// TASK 2
	vector<Name> names = inputNames();
	vector<Data> nameData;

	// Load the names into a data structure and insert into the AVL tree
	for (int i = 0; i < 10; i++) {
		Data current;
		current.key = randNums[i];
		current.name = names[i];
		tree.AVL_Insert(current);
	}
	// Print the AVL tree
	cout << "TASK 2" << endl;
	tree.AVL_Print();

	cout << endl;

	// TASK 3
	// In-order traversal will list in ascending order of keys
	cout << "TASK 3" << endl;
	tree.AVL_Traverse(print);

	cout << endl;

	// TASK 4
	// Perform 10 linear searches and remove elements as they are printed
	cout << "TASK 4" << endl;
	for (int i = 0; i < 10; i++) {
		vector<Name>::iterator min = names.begin(); // The minimum name for this iteration
		// Linear search over the array
		for (vector<Name>::iterator j = names.begin()++; j < names.end(); j++) {
			// Note: The provided < overloader is incorrect so we have to flip the operands
			if (*min < *j) {
				min = j;
			}
		}
		// Print the name
		min->display();
		// Remove the element from the vector so that we don't analyse it again
		names.erase(min);
	}
	cout << endl;

	// TASK 5
	// Use a custom created breadth first traversal
	cout << "TASK 5" << endl;
	tree.AVL_Traverse_Breadth(print);

	return 0;
}
