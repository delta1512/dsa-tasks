#ifndef AVL_ADT_EXT_H_
#define AVL_ADT_EXT_H_

// TASK 5
template<class TYPE, class KTYPE>
class AVLTreeExt : public AvlTree<TYPE, KTYPE> {

  protected:
    void  _breadth_first_traversal  (void (*process)(TYPE dataProc),
                              NODE<TYPE>    *root);
  public:
  	void  AVL_Traverse_Breadth (void (*process)(TYPE  dataProc));

};


template <class TYPE, class KTYPE>
void  AVLTreeExt<TYPE, KTYPE>::AVL_Traverse_Breadth
                  (void (*process)(TYPE dataProc)) {
	_breadth_first_traversal(process, this->tree);
	return;
}

// Perform a breadth-first traversal using a queue
template <class TYPE, class KTYPE>
void  AVLTreeExt<TYPE, KTYPE>::_breadth_first_traversal
                  (void(*process)(TYPE dataproc),
                  NODE<TYPE> *root) {
	if (root) {
    // If a root exists, then load it into the queue
    queue<NODE<TYPE>*> BFSQueue;
    BFSQueue.push(root);
    // Process the front node and then add its subtrees if they exist
    while (!BFSQueue.empty()) {
      NODE<TYPE>* current = BFSQueue.front();
      process(current->data);
      BFSQueue.pop();
      if (current->left) {
        BFSQueue.push(current->left);
      }
      if (current->right) {
        BFSQueue.push(current->right);
      }
    }
	}
	return;
}

#endif /* AVL_ADT_EXT_H_ */
