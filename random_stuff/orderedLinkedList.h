#include <iostream>

#include "linkedList.h"


#ifndef H_orderedLinkedList
#define H_orderedLinkedList


using namespace std;


template <class Type>
class orderedLinkedList: public linkedListType<Type> {
  public:
    orderedLinkedList() : linkedListType<Type>() {};
    void insertFirst(const Type& newItem);
    void insertLast(const Type& newItem);
    void deleteNode(const Type& deleteItem);
    void show();
};


template <class Type>
void orderedLinkedList<Type>::insertFirst(const Type& newItem) {
  nodeType<Type>* newNode = new nodeType<Type>();
  newNode->info = newItem;
  newNode->link = linkedListType<Type>::first;
  linkedListType<Type>::first = newNode;
  linkedListType<Type>::count++;
}


template <class Type>
void orderedLinkedList<Type>::insertLast(const Type& newItem) {
  nodeType<Type>* newNode = new nodeType<Type>();
  newNode->info = newItem;
  newNode->link = NULL;

  nodeType<Type>* tempNode = linkedListType<Type>::first;
  for (int i = 0; i < linkedListType<Type>::count-1; i++) {
    tempNode = tempNode->link;
  }
  tempNode->link = newNode;
  linkedListType<Type>::count++;
}


template <class Type>
void orderedLinkedList<Type>::deleteNode(const Type& deleteItem) {
  nodeType<Type>* prevNode = NULL;
  nodeType<Type>* nextNode = linkedListType<Type>::first;
  bool found = false;

  for (int i = 0; i < linkedListType<Type>::count; i++) {
    if (nextNode->info == deleteItem) {
      if (prevNode == NULL) {
        linkedListType<Type>::first = linkedListType<Type>::first->link;
        found = true;
      } else {
        prevNode->link = nextNode->link;
        found = true;
      }
      break;
    }
    prevNode = nextNode;
    nextNode = nextNode->link;
  }
  if (found) {
    linkedListType<Type>::count--;
  }
}


template <class Type>
void orderedLinkedList<Type>::show() {
  nodeType<Type>* tempNode = linkedListType<Type>::first;
  for (int i = 0; i < linkedListType<Type>::count; i++) {
    cout << tempNode->info << " ";
    tempNode = tempNode->link;
  }
  cout << endl;
}



#endif
