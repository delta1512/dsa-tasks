#include <iostream>
#include <ctime>
#include <cstdlib>

using namespace std;

int main() {
  srand(time(NULL));

  int n = 50;
  int s = 0;

  for (int i = 0; i < n; i++) {
    s++;
  }

  if (n < 100) {
    s *= n*n;
  } else {
    s += 10;
  }

  cout << s << endl;

  return 0;
}
