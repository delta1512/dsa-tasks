#include <queue>
#include <iostream>

using namespace std;

int main() {
  priority_queue<double, vector<double>, greater<double>> test;
  test.push(0.69);
  test.push(0.9);
  cout << test.top() << endl;

  return 0;
}
