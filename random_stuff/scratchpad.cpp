#include<iostream>
#include<list>
#include<iterator>

using namespace std;


int main() {
    list<int> intList;
    ostream_iterator<int> screen(cout, " ");
    list<int>::iterator listIt;
    intList.push_back(5);
    intList.push_front(23);
    intList.pop_back();
    intList.push_back(35);
    intList.push_front(34);

    copy(intList.begin(), intList.end(), screen);

    cout << endl;

    listIt = intList.begin();
    intList.insert(listIt,76);
    intList.pop_back();
    ++listIt;
    intList.erase(listIt);
    intList.push_front(2 * intList.back());
    copy(intList.begin(), intList.end(), screen);
    cout << endl;

  return 0;
}
