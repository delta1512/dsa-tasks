#include <iostream>
#include <stack>

using namespace std;

class chair {
  int wheels;

  public:
    int size;

    chair(int s) {
      size = s;
      wheels = 4;
    };

    void showchair() {
      cout << "size " << size << endl;
      cout << "wheels " << wheels << endl;
    };

    ~chair() {
      cout << "Chair has been destroyed" << endl;
    }
};

int main() {
  stack<chair> mystack = stack<chair>();

  mystack.push(chair(10));
  mystack.push(chair(2));
  mystack.push(chair(100));

  mystack.top().showchair();

  mystack.pop();

  cout << mystack.size() << endl;

  return 0;
}
