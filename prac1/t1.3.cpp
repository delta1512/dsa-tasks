#include <iostream>
#include <cstdlib>
#include <ctime>
#include <vector>

using namespace std;

int getRand(int a, int b, int& nRand);
void chooseRandNums(int* numList, int& nRand);


int main() {
  // Seed the random number generator
  srand(time(NULL));

  // Initialise a vector of int lists (int ptrs)
  vector<int*> numbers;
  // nRand is how many random calls have been made
  int nRand=0, current;
  int* currentNums;

  // Initialise the vector of numbers
  for (int i = 0; i < 10; i++) {
    currentNums = new int[9];
    for (int j = 0; j < 9; j++) {
      numbers.push_back(currentNums);
    }
  }

  // For every list of numbers
  for (int i = 0; i < 10; i++) {
    // Choose the numbers for the array
    chooseRandNums(numbers[i], nRand);
    // Print the array and random calls
    for (int j = 0; j < 9; j++) {
      cout << numbers[i][j] << " ";
    }
    cout << " Random calls: " << nRand << endl;
    // Reset the random count to 0
    nRand = 0;
  }

  return 0;
}


int getRand(int a, int b, int& nRand) {
  // nRand counts the number of random numbers have been generated
  nRand++;
  return rand() % (b-a+1) + a;
}


void chooseRandNums(int* numList, int& nRand) {
  // Ensure the list is initialised to all-zeros
  for (int i = 0; i < 9; i++) {
    numList[i] = 0;
  }

  // Get a random number to start with
  int current = getRand(0, 8, nRand);
  for (int i = 1; i <= 6; i++) {
    // Keep getting random numbers until a free space is found at that index
    while (numList[current] != 0) {
      current = getRand(0, 8, nRand);
    }
    // Set the current index to the iteration of the for loop
    numList[current] = i;
  }
}
