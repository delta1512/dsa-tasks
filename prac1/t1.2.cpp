#include <iostream>
#include <cstdlib>
#include <ctime>

using namespace std;

int getRand(int a, int b, int& nRand);


int main() {
  // Seed the random number generator
  srand(time(NULL));

  // initialise an all-zero array
  int final[9] = {0, 0, 0, 0, 0, 0, 0, 0, 0};
  // nRand is how many random calls have been made
  int nRand=0, current;

  // Start with one random number
  current = getRand(0, 8, nRand);
  for (int i = 1; i <= 6; i++) {
    // Keep getting random numbers until a free space is found at that index
    while (final[current] != 0) {
      current = getRand(0, 8, nRand);
    }
    // Set the current index to the iteration of the for loop
    final[current] = i;
  }

  // Print out all the elements of the array
  for (int i = 0; i < 9; i++) {
    cout << final[i] << " ";
  }
  cout << " Random calls: " << nRand << endl;

  return 0;
}


// Gets a random number between a and b (inclusive)
int getRand(int a, int b, int& nRand) {
  // nRand counts the number of random numbers have been generated
  nRand++;
  return rand() % (b-a+1) + a;
}
