#include <iostream>

using namespace std;

template <class Type>
void funcExp(Type list[], int size) {
  Type x = list[0];
  Type y = list[size - 1];
  for (int j = 1; j < size; j++) {
    if (x < list[j])
      x = list[j];

    if (y > list[size - 1 -j])
      y = list[size - 1 -j];
  }
  cout << x << endl;
  cout << y << endl;
}

int main() {
  string strList[] = {"One", "Hello", "Four", "Three", "How", "Six"};
  int list[10] = {5,3,2,10,4,19,45,13,61,11};

  funcExp(strList, 6);
  funcExp(list, 10);

  return 0;
}
