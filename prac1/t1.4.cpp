#include <iostream>
#include <cstdlib>
#include <ctime>
#include <vector>

using namespace std;

int getRand(int a, int b, int& nRand);
void chooseRandNums(int* numList, int& nRand);


int main() {
  // Seed the random number generator
  srand(time(NULL));

  // Initialise a vector of int lists (int ptrs)
  vector<int*> numbers;
  // nRand is how many random calls have been made
  int nRand=0, current;
  int* currentNums;

  // Initialise the vector of numbers
  for (int i = 0; i < 10; i++) {
    currentNums = new int[9];
    for (int j = 0; j < 9; j++) {
      numbers.push_back(currentNums);
    }
  }

  // For every list of numbers
  for (int i = 0; i < 10; i++) {
    // Choose the numbers for the array
    chooseRandNums(numbers[i], nRand);
    // Print the array and random calls
    for (int j = 0; j < 9; j++) {
      cout << numbers[i][j] << " ";
    }
    cout << " Random calls: " << nRand << endl;
    // Reset the random count to 0
    nRand = 0;
  }

  return 0;
}


int getRand(int a, int b, int& nRand) {
  // nRand counts the number of random numbers have been generated
  nRand++;
  return rand() % (b-a+1) + a;
}


void chooseRandNums(int* numList, int& nRand) {
  int currentRand, chosenIndex=0;

  // Initialise the list to all-zeroes
  for (int i = 0; i < 9; i++) {
    numList[i] = 0;
  }

  // For every number 1-6
  for (int i = 1; i <= 6; i++) {
    // Get a random number, doesn't matter how large, an example here shows that
    // The upper limit must be 8 at a minimum
    currentRand = getRand(0, 100, nRand);
    // Traverse the array's free spaces for currentRand steps
    while (currentRand != 0 || numList[chosenIndex] != 0) {
      // If it is a free space, decrement the random counter
      // This prevents determinstic placement and placement over non-zero values
      if (numList[chosenIndex] == 0) {
        currentRand--;
      }
      // Increment the index no matter what
      chosenIndex++;
      // Modulo it with the length of the number list (9)
      chosenIndex = chosenIndex % 9;
    }
    // Set the chosen index to the iteration of the for loop
    numList[chosenIndex] = i;
  }
}
