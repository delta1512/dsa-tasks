/*	Shell Sort
	Revised by: Dr Dongmo Zhang
	Date:       15/9/2005

    Origin by: G & F
	Brooks/Cole
	A division of Thomson Learning
	Copyright(c) 2001. All Rights Reserved
*/
#include <iostream>
#include <iomanip>

using namespace std;

//	Prototype Declarations
void shellSort (int list[ ], int last );


/*
int main ( void )
{
//	Local Declarations
	int i;
	int	list[ MAX_ARY_SIZE ] = { 89, 72, 3,  15, 21,
	                             57, 61, 44, 19, 98,
	                              5, 77, 39, 59, 61 };

//	Statements
	cout <<  "Unsorted array: " ;
	for ( i = 0; i < MAX_ARY_SIZE; i++ )
		cout << setw(3) << list[ i ] ;

	shellSort (list, MAX_ARY_SIZE - 1);

	cout <<  "\nSorted array:   " ;
	for ( i = 0; i < MAX_ARY_SIZE; i++ )
		cout << setw(3) <<  list[ i ] ;
	cout << endl ;
	return 0;
}	// main
*/

/*	==================== shellSort ====================
	List[0], list[1], ..., list[last] are sorted in place.
	After the sort, their keys will be in order, list[0].key
	<= list[1].key <= ... <= list[last].key.
	   Pre   list is an unordered array of integers
	         last is index to last element in array
	   Post  list is ordered
*/

void shellSort (int list [],
                int last)
{
//	Local Definitions
	int hold;
	int incre;
	int curr;
	int walker;

//	Statements
	incre = last / 2;
	while (incre != 0)
	   { //cout << "\nk=" << incre;
	    for (curr = incre; curr <= last; curr++)
	       {
	        hold = list [curr];
	        walker = curr - incre;
	        while (walker >= 0 && hold < list [walker])
	           {
	            // Move larger element up in list
	            list [walker + incre] = list [walker];
	            //  Fall back one partition
	            walker = (walker - incre);
	           } //  while
	        // Insert hold in proper relative position
	        list [walker + incre] = hold;
	       } // for
	    //  End of pass--calculate next increment.
	    incre = incre / 2;
	   } // while
	return;
} //  shellSort

/*	Results
	Unsorted array:  89 72  3 15 21 57 61 44 19 98  5 77 39 59 61
	Sorted array:     3  5 15 19 21 39 44 57 59 61 61 72 77 89 98
*/
