/*	Test driver for sorting.
	Note: Heap sort uses reheapUp and reheapDown
	which were defined in Chapter 9.

	Written by: G & F
	Date:       2/98

	Revised     5/99  Converted to C++

	Brooks/Cole
	A division of Thomson Learning
	Copyright(c) 2001. All Rights Reserved
*/
#include <iostream>
#include <iomanip>

using namespace std;


//	Prototype Declarations
void swap        (int *heap,    int  x1,   int  x2);
void heapSort    (int  list[ ], int  last);
void reheapUp    (int  heap[ ], int  newElem);
void reheapDown  (int  heap[ ], int  current, int last);
void swap        (int *heap,    int x1,       int x2) ;

/*
int main (void)
{
//	Prototype Declarations

//	Local Declarations
	int i;
	int	ary[ MAX_ARY_SIZE ] = { 89, 72, 3,  15, 21,
	                            57, 61, 44, 19, 98,
	                             5, 77, 39, 59, 61
	                          }; // ary
//	Statements
	cout << "Unsorted array: " ;
	for (i = 0; i < MAX_ARY_SIZE; i++)
	    cout << setw(3) << ary[ i ];

	heapSort (ary, MAX_ARY_SIZE - 1);

	cout <<  "\nSorted array:   ";
	for (i = 0; i < MAX_ARY_SIZE; i++)
	    cout << setw(3) <<  ary[ i ];
	cout << endl ;
	return 0;
}	// main
*/

/*	====================== heapSort =======================
	Sort an array, list[0...last], using a heap.
	   Pre   list must contain at least one item
	         last contains index to last element in the list
	   Post  list has been rearranged smallest to largest
*/
void heapSort  (int  list[],
                int  last)
{
//	Local Definitions
	int sorted;
	int holdData;
	int walker;

//	Statements
	// Create heap
	for (walker = 1; walker <= last; walker++)
	    reheapUp (list, walker);

//	Heap created. Now sort it.
	sorted = last;
	while (sorted > 0)
	   {
	    holdData      = list[0];
	    list[0]       = list[sorted];
	    list[sorted]  = holdData;
	    sorted--;
	    reheapDown (list, 0, sorted);
	   } // while
	return;
}	// heapSort

/*	==================== reheapUp ====================
	Move last entry to correct location in heap array.
	   Pre   heap is array; last is index to last element.
	         (the one being inserted or most recently moved)
	   Post  the array is a valid heap.
*/
void reheapUp (int  *heap,
               int   heapLast)
{
//	Prototype Declarations
void swap (int *heap, int x1, int x2) ;

/*	Local Declarations */
	int parent ;
	int child ;

//	Statements
	if (heapLast != 0)
	   {
	    // if not at root of heap
	    parent = (heapLast - 1) / 2 ;
	    child  =  heapLast ;
	    if (heap[child] > heap[parent])
	       {
	        // child is greater than parent
	        swap     (heap, child, parent) ;
	        reheapUp (heap, parent) ;
	       } // if
	   } // last != 0
	return ;
}	// reheapUp

/*	==================== reheapDown ====================
	Move root of tree or subtree down by replacing it with the
	larger of its two children.
	   Pre   subtrees are valid heaps; the key at top of heap
	         may be smaller than either of its children.
	   Post  The heap structure is valid.
*/
void reheapDown (int *heap,
                 int  current,
                 int  heapLast)
{
//	Local Declarations
	int parent ;
	int lftChild ;
	int rgtChild ;
	int maxChild ;

//	Statements
	parent     = current ;
	lftChild   = (current * 2) + 1 ;
	rgtChild   = lftChild  + 1 ;

	if (lftChild <= heapLast)
	   {
	    // There is at least one child
	    if (lftChild  == heapLast)
	       // Only a left child
	       maxChild = lftChild ;
	    else
	       // Determine which is child is larger
 	       if (heap[lftChild] > heap[rgtChild])
 	           maxChild = lftChild ;
 	       else
 	           maxChild = rgtChild ;

 	    // Now determine if parent > children
	    if (heap[parent] < heap[maxChild])
	       {
	        // parent < children
	        swap       (heap, parent,   maxChild) ;
	        reheapDown (heap, maxChild, heapLast) ;
	       } // if there is at least one child
	    else
	        // At leaf
	        return ;
	   } // lftChild <= 1

	return ;
}	// reheapDown

/*	==================== swap ====================
	This function exchanges two elements in the heap.
	   Pre   heap is an array heap
	         x1 andx2 identify heap elements to be exchanged
	   Post  entries in heap have been exchanged.
*/
void swap (int *heap,
           int  x1,
           int  x2)
{
//	Local Declarations
	int  temp ;

//	Statements
	temp      = heap[x1] ;
	heap[x1]  = heap[x2] ;
	heap[x2]  = temp ;

	return ;
}	// swap

/*	Results
	Unsorted array:  89 72  3 15 21 57 61 44 19 98  5 77 39 59 61
	Sorted array:     3  5 15 19 21 39 44 57 59 61 61 72 77 89 98
*/
