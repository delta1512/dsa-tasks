#include <iostream>
#include <ctime>
#include <cstdlib>
#include <sys/time.h>

#include "heap.cpp"
#include "quick.cpp"
#include "Shell.cpp"


using namespace std;


long int getMillis();
int getRand(int a, int b);


int main() {
  srand(time(NULL));
  long int startMilli, endMilli;
  int startSize = 10000;
  int endSize = 100000000;

  for (int i = startSize; i < endSize; i *= 10) {
    long int startMilli, endMilli;
    int* data = new int[i];
    int* datacpy = new int[i];
    for (int x = 0; x < i; x++) {
      data[x] = getRand(0, i);
      datacpy[x] = data[x];
    }
    cout << endl << "Testing array size " << i << endl;

    // Quicksort
    cout << "Quick sort running... ";
    QuickSort qs(data, i);
    startMilli = getMillis();
    qs.sort(0, i - 1);
    endMilli = getMillis();
    cout << endMilli - startMilli << "ms" << endl;
    //qs.print();

    // Shell sort
    cout << "Shell sort running... ";
    startMilli = getMillis();
    shellSort(data, i - 1);
    endMilli = getMillis();
    cout << endMilli - startMilli << "ms" << endl;

    // Heap sort
    cout << "Heap sort running... ";
    startMilli = getMillis();
    heapSort(datacpy, i - 1);
    endMilli = getMillis();
    cout << endMilli - startMilli << "ms" << endl;

    delete[] data;
    delete[] datacpy;
  }

  return 0;
}


long int getMillis() {
  struct timeval currentTime;
  gettimeofday(&currentTime, NULL);
  return currentTime.tv_sec * 1000 + currentTime.tv_usec / 1000;
}


int getRand(int a, int b) {
  return rand() % (b-a+1) + a;
}
