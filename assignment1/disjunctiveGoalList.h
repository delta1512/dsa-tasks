#include "goalList.h"
#include "state.h"

#ifndef disjunctiveGoalList_H
#define disjunctiveGoalList_H

using namespace std;

class DisjunctiveGoalList : public GoalList {
  public:
    DisjunctiveGoalList() : GoalList() {};
    bool isSatisfied(State* gameState);
    void getActionHeuristic(State* gameState, Action* act);
};


#endif
