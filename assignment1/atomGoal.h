#include <iostream>

#include "goal.h"

using namespace std;

#ifndef atomGoal_H
#define atomGoal_H

class AtomGoal : public Goal {
  public:
    AtomGoal(State* g) : Goal(
      getRand(1, g->getNums()),
      getRand(0, g->getSize()-1),
      getRand(0, g->getSize()-1)
    ) {};
    AtomGoal(int a, int b, int c) : Goal(a, b, c) {};

    void showHumanReadable();
    bool isValid(State* gameState);
    bool isSatisfied(State* gameState);
    double getHeuristic(State* gameState);
};

#endif
