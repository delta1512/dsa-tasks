#ifndef action_H
#define action_H


using namespace std;


class Action {
  int fromCol;
  int toCol;
  double heuristic;

  public:
    Action() {}; // Empty constructor (do not construct like this unless it is a placeholder)
    Action(int f, int t) : fromCol(f), toCol(t) {};
    Action(int f, int t, double hu) : fromCol(f), toCol(t), heuristic(hu) {};

    bool operator>(const Action& otherAct) const {return heuristic > otherAct.getHeuristic();};
    bool operator==(Action& otherAct);

    int getFromCol() {return fromCol;};
    int getToCol() {return toCol;}
    double getHeuristic() const {return heuristic;};
    void setHeuristic(double hu) {heuristic = hu;};
    bool isReverseOf(Action& otherAct);
    void show();
    void showHumanReadable();
};


#endif
