#include "goalList.h"
#include "state.h"

#ifndef conjunctiveGoalList_H
#define conjunctiveGoalList_H

using namespace std;

class ConjunctiveGoalList : public GoalList {
  public:
    ConjunctiveGoalList() : GoalList() {};
    bool isSatisfied(State* gameState);
    void getActionHeuristic(State* gameState, Action* act);
};


#endif
