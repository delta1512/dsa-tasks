/*********** Declaration*******
I hereby certify that no part of this assignment has been copied from
any other student’s work or from any other source. No part of the code
has been written/produced for me by another person or copied from any
other source.

I hold a copy of this assignment that I can produce if the original is
lost or damaged.
**************************/
#include "goal.h"


// Constructor sets up the tuple
// The tuple holds the coordinates and directions of the goal
// They hold different things depending on an atom or neighbour goal
Goal::Goal(int a, int b, int c) {
  goalTuple[0] = a;
  goalTuple[1] = b;
  goalTuple[2] = c;
}


// Converts the goal to a string by simply printing the tuple array
string Goal::toString() {
  // Convert the direction provided in a neighbour goal if any
  string directionOrCoord;
  if (goalTuple[1] < 0) {
    directionOrCoord = DIRECTION_CHARS[4 + goalTuple[1]];
  } else {
    directionOrCoord = to_string(goalTuple[1]);
  }

  return "(" + to_string(goalTuple[0]) + ", " + directionOrCoord + ", " + to_string(goalTuple[2]) + ")";
}


// Show the non-human-readable presentation of the goal
void Goal::show() {
  cout << toString() << endl;
}
