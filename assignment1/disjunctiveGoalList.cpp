/*********** Declaration*******
I hereby certify that no part of this assignment has been copied from
any other student’s work or from any other source. No part of the code
has been written/produced for me by another person or copied from any
other source.

I hold a copy of this assignment that I can produce if the original is
lost or damaged.
**************************/
#include "disjunctiveGoalList.h"


// Runs through all the goals to see if any one of them are satisfied
bool DisjunctiveGoalList::isSatisfied(State* gameState) {
  for (list<Goal*>::iterator i = goalSet.begin(); i != goalSet.end(); i++) {
    if ((*i)->isSatisfied(gameState)) {
      cout << "The satisfied goal is: ";
      (*i)->showHumanReadable();
      return true;
    }
  }
  return false;
}


// Gets the heuristic for the action and applies it
// Takes the net minimum of all the heuristics and hence focuses on that goal
void DisjunctiveGoalList::getActionHeuristic(State* newGameState, Action* act) {
  double min = 1.0;
  for (list<Goal*>::iterator i = goalSet.begin(); i != goalSet.end(); i++) {
    double tmp = (*i)->getHeuristic(newGameState);
    if (tmp < min) {
      min = tmp;
    }
  }
  act->setHeuristic(min);
}
