#include <iostream>
#include <string>
#include <math.h>

#include "constants.h"
#include "randomness.h"
#include "state.h"

#ifndef goal_H
#define goal_H

class Goal {
  protected:
    int goalTuple[3];

  public:
    Goal() {};
    Goal(int a, int b, int c);
    string toString();
    void show();
    double linDist(int x0, int x1, int y0, int y1) {
      return sqrt(pow((double)x0-x1, 2) + pow((double)y0-y1, 2));
    };
    virtual void showHumanReadable() = 0;
    virtual bool isValid(State* gameState) = 0;
    virtual bool isSatisfied(State* gameState) = 0;
    virtual double getHeuristic(State* gameState) = 0;
};


#endif
