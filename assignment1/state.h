#include <vector>
#include <math.h>

#include "constants.h"
#include "action.h"


#ifndef state_H
#define state_H

using namespace std;


class State {
  int** board;
  int size;
  int nums;

  int top(int col);
  bool isEmpty(int col);
  int popFromCol(int col);

  public:
    double maxHeuristic;

    State() {}; // Empty constructor (do not construct like this unless it is a placeholder)
    State(State* s); // Copy constructor
    State(int s, int n);
    State(int s, int n, int* boardDef);
    void initBoard();
    void clearBoard();
    void randomiseBoard();
    int getSize() {return size;};
    int getNums() {return nums;};
    int** getBoard() {return board;}
    void find(int num, int& x, int& y);
    int topTile(int col);
    void showBoard();
    void pushToCol(int val, int col); // Must remain public for manual board description
    bool isValidAction(Action& a);
    void performAction(Action& a);
    void reverseAction(Action& a);
    void getPossibleMoves(vector<Action>& actionList);
    string getHash();
    ~State();
};


#endif
