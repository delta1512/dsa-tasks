#include <iostream>

#include "goal.h"

using namespace std;

#ifndef neighbourGoal_H
#define neighbourGoal_H

class NeighbourGoal : public Goal {
  public:
    NeighbourGoal(State* g);
    NeighbourGoal(int a, int b, int c) : Goal(a, b, c) {};

    void showHumanReadable();
    bool isValid(State* gameState);
    bool isSatisfied(State* gameState);
    double getHeuristic(State* gameState);
};

#endif
