/*********** Declaration*******
I hereby certify that no part of this assignment has been copied from
any other student’s work or from any other source. No part of the code
has been written/produced for me by another person or copied from any
other source.

I hold a copy of this assignment that I can produce if the original is
lost or damaged.
**************************/
#include <iostream>

#include "action.h"


// Equality operator
bool Action::operator==(Action& otherAct) {
  return fromCol == otherAct.getFromCol() && toCol == otherAct.getToCol();
}


// Prints out just the action tuple (to and from values)
void Action::show() {
  cout << "(" << fromCol << ", " << toCol << ")" << endl;
}


// Shows a human readable form for the user to read
void Action::showHumanReadable() {
  cout << "Moving from column " << fromCol << " to column " << toCol << endl;
}


// Checks if the action provided is the reverse of the current one
bool Action::isReverseOf(Action& otherAct) {
  return fromCol == otherAct.getToCol() && toCol == otherAct.getFromCol();
}
