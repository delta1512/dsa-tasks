#include <unordered_set>
#include <string>
#include <vector>
#include <list>
#include <queue>

#include "state.h"
#include "action.h"
#include "goalList.h"
#include "randomness.h"

using namespace std;

#ifndef solver_H
#define solver_H


class Solver {
  list<Action> plan;
  State* mainState;
  GoalList* finalGoal;
  unordered_set<string> hashSet;

  public:
    // Garbage collection of mainState and finalGoal is handled in destructor
    Solver(State* s, GoalList* g) : mainState(s), finalGoal(g) {};
    void addToPlan(Action act);
    void printPlan();
    bool hashExists(string hash);
    bool bestFirstSearch(State* node, int maxRecurse);
    void getHeuristicActions(
      State* currentState,
      priority_queue<Action, vector<Action>, greater<Action>>& q
    );

    void randomSolver(int maxSteps=100);
    void BFSSolver(int maxRecurse=100);

    ~Solver();
};


#endif
