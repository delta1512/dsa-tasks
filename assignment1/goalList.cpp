/*********** Declaration*******
I hereby certify that no part of this assignment has been copied from
any other student’s work or from any other source. No part of the code
has been written/produced for me by another person or copied from any
other source.

I hold a copy of this assignment that I can produce if the original is
lost or damaged.
**************************/
#include <iostream>

#include "goalList.h"
#include "randomness.h"


// Push a goal new to the goal list
void GoalList::addGoal(Goal* goal) {
  goalSet.push_back(goal);
}


// Show all the goals in the list
void GoalList::showGoals() {
  for (list<Goal*>::iterator i = goalSet.begin(); i != goalSet.end(); i++) {
    (*i)->showHumanReadable();
  }
}


// Checks whether all the goals in the list are valid or not
bool GoalList::isValid(State* gameState) {
  for (list<Goal*>::iterator i = goalSet.begin(); i != goalSet.end(); i++) {
    if (!(*i)->isValid(gameState)) {
      return false;
    }
  }
  return true;
}


GoalList::~GoalList() {
  for (list<Goal*>::iterator i = goalSet.begin(); i != goalSet.end(); i++) {
    delete *i;
  }
}
