/*********** Declaration*******
I hereby certify that no part of this assignment has been copied from
any other student’s work or from any other source. No part of the code
has been written/produced for me by another person or copied from any
other source.

I hold a copy of this assignment that I can produce if the original is
lost or damaged.
**************************/
#include "conjunctiveGoalList.h"


// Runs through the goal list to see if all of the goals are satisfied
bool ConjunctiveGoalList::isSatisfied(State* gameState) {
  for (list<Goal*>::iterator i = goalSet.begin(); i != goalSet.end(); i++) {
    if (!(*i)->isSatisfied(gameState)) {
      return false;
    }
  }
  return true;
}


// Gets the heuristic for an action
// Takes the net average of all the heuristics from all the goals
void ConjunctiveGoalList::getActionHeuristic(State* newGameState, Action* act) {
  double sum = 0.0;
  for (list<Goal*>::iterator i = goalSet.begin(); i != goalSet.end(); i++) {
    sum += (*i)->getHeuristic(newGameState);
  }
  act->setHeuristic(sum/(double)goalSet.size());
}
