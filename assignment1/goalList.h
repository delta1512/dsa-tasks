#include <list>

#include "goal.h"
#include "state.h"
#include "action.h"

#ifndef goalList_H
#define goalList_H

using namespace std;

class GoalList {
  protected:
    list<Goal*> goalSet;

  public:
    GoalList() {};
    void addGoal(Goal* goal);
    void showGoals();
    bool isValid(State* gameState);
    virtual bool isSatisfied(State* gameState) = 0;
    virtual void getActionHeuristic(State* gameState, Action* act) = 0;
    ~GoalList();
};


#endif
