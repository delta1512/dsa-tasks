/*********** Declaration*******
I hereby certify that no part of this assignment has been copied from
any other student’s work or from any other source. No part of the code
has been written/produced for me by another person or copied from any
other source.

I hold a copy of this assignment that I can produce if the original is
lost or damaged.
**************************/

#include "idea.h"

// Initialise the static variables
unordered_set<int> Idea::ids;
int Idea::nId = 0;


// Returns a unique id
// It uses a hashmap to store and check already used IDs with O(1) complexity
// Use preset to attempt to use your own ID, else it will return an available ID
int Idea::getNewId(int preset=0) {
  // If there are too many elements in the database, raise an error
  assert(ids.size() < INT_MAX);
  // If we want to set a custom id
  if (preset > 0) {
    // If it is already taken, just get a brand new id
    if (!ids.insert(preset).second) {
      return getNewId();
    } else {
      return preset;
    }
  } else {
    // Keep iterating over the valid ids until there is one free
    while (!ids.insert(++nId).second) {
      nId = nId % INT_MAX;
    }
    return nId;
  }
}


// Normal constructor with supplied variables
// ID will automatically be set
Idea::Idea(string p, vector<string>* k, string c) {
  id = getNewId();

  // Copy the elements from the list
  for (vector<string>::iterator i = k->begin(); i != k->end(); i++) {
    keywords.push_back(*i);
  }

  content = c;
}


// Constructor for user input
// ID will automatically be set
Idea::Idea() {
  id = getNewId();

  cout << "Who made the idea? ";
  getline(cin, proposer);

  string tmp;
  // Fix off-by one bug where "done" is added to the list
  cout << "Input a keyword (type 'done' to exit): ";
  getline(cin, tmp);
  while (tmp != "done") {
    keywords.push_back(tmp);
    cout << "Input a keyword (type 'done' to exit): ";
    getline(cin, tmp);
  }
  cout << "Write the idea content: ";
  getline(cin, content);
}


// Print the idea in a human readable format
void Idea::show() {
  cout << "Idea no.: " << id << endl;
  cout << "Creator: " << proposer << endl;
  cout << "Keywords: ";
  for (int i = 0; i < keywords.size(); i++) {
    cout << keywords[i];
    if (i != keywords.size()-1) {
      cout << ", ";
    }
  }
  cout << endl << "Content: " << content << endl << endl;
}


// Will copy all the words in keywords and content into the provided set
// using unordered_set to ensure only unique words.
void Idea::getAllWordsInIdea(unordered_set<string>& words) {
  // Iterate through all the keywords first
  for (int i = 0; i < keywords.size(); i++) {
    words.insert(keywords[i]);
  }

  int i = 0;
  while (i < content.size()) {
    string currentStr = "";
    // Use ASCII numbers to check for alphabetical characters
    while ((content[i] <= 122 && content[i] >= 97) ||
          (content[i] <= 90 && content[i] >= 65)) {
            currentStr += content[i];
            i++;
    }
    // Only if we actually found a word, add it to the list
    if (currentStr.size() > 0) {
      words.insert(currentStr);
    }
    i++;
  }
}


// Returns whether the kwd is in the keywords of the idea
bool Idea::isKeyword(string kwd) {
  for (int i = 0; i < keywords.size(); i++) {
    if (keywords[i] == kwd) {
      return true;
    }
  }
  return false;
}


// Checks to see if a keyword is in the text content of an idea
bool Idea::isInContent(string kwd) {
  return find(kwd, content);
}


// Finds the position+1 where kwd starts in the content
// else returns 0
// Algorithm scans linearly to find a similar starting character and
// if it does, it starts scanning both the kwd and content together and iterating
// along both strings until the entire kwd is found.
// Note: The reason this is external is because it is a general type of algorithm
int find(string kwd, string content) {
  for (int i = 0; i < content.size(); i++) {
    if (content[i] == kwd[0]) {
      i++;
      int foundPos = i;
      for (int c = 1; c < kwd.size(); c++) {
        // If the string no longer matches, exit search
        if (content[i] != kwd[c]) {
          break;
        // Else if we have iterated over the whole kwd
        } else if (c == kwd.size()-1) {
          // Return found
          return foundPos;
        }
        // Iterate over the content
        i++;
      }
    }
  }
  return 0;
}


// Sanitises a string when serialising
// Prevents ideas from crashing the serialisation process
string Idea::sanitise(string str) {
  for (int i = 0; i < str.length(); i++) {
    // If a character is a delimiter, replace it with something else
    if (str[i] == DELIMITER) {
      str[i] = ALT_DELIM;
    }
  }
  return str;
}


// Turns an idea into one long string for saving to a file or copying to another idea
// Check constants.h for info regarding how serialisations are structured
string Idea::serialise() {
  string final = START_IDEA_DELIM;

  final += to_string(id);
  final += DELIMITER;
  final += sanitise(proposer);
  final += DELIMITER;
  int count = 1;
  for (int i = 0; i < keywords.size(); i++) {
    final += sanitise(keywords[i]);
    if (i != keywords.size()-1) {
      final += ",";
    }
  }
  final += DELIMITER;
  final += sanitise(content);

  final += END_IDEA_DELIM;
  return final;
}


// Convert a serialised string of an idea into an actual idea object
// cpy determines whether or not this is a deserialisation for the copy constructor
void Idea::deSerialise(string serial, bool cpy) {
  // Start scanning after the starting delimiter
  int seek = START_IDEA_DELIM.length();

  // ID
  string idString = "";
  do {
    idString += serial[seek];
    seek++;
  } while (serial[seek] != DELIMITER);

  int tmpId = stoi(idString);
  // If this is a copy, don't attempt to get a new ID
  if (cpy) {
    id = tmpId;
  } else {
    id = getNewId(tmpId);
  }
  // ---

  seek++; // Move to the start of the proposer name

  // PROPOSER
  proposer = "";
  do {
    proposer += serial[seek];
    seek++;
  } while (serial[seek] != DELIMITER);
  // ---

  seek++;

  // KEYWORDS
  do {
    string tmpkwd = "";
    do {
      tmpkwd += serial[seek];
      seek++;
    } while (serial[seek] != ',' && serial[seek] != DELIMITER);
    keywords.push_back(tmpkwd);
    if (serial[seek] == ',') {
      seek++;
    }
  } while (serial[seek] != DELIMITER);
  // ---

  seek++;

  int seekEnd = serial.length()-END_IDEA_DELIM.length();
  // CONTENT
  content = "";
  do {
    content += serial[seek];
    seek++;
  } while (serial[seek] != DELIMITER && seek != seekEnd);
  // ---
}
