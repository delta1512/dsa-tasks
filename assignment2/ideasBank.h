#include <iostream>
#include <fstream>
#include <list>
#include <vector>
#include <unordered_map>
#include <unordered_set>

#include "idea.h"
#include "AVL_ADT.h"


#ifndef IDEABANK_H
#define IDEABANK_H

using namespace std;


// Node structure for storing the indices in the AVL tree
struct wordIndex {
  string key;
  list<int> ids;
};


class IdeasBank {
  AvlTree<wordIndex,string> index;
  unordered_map<int,Idea> bank;

  void reIndexAdd(Idea& idea);
  void reIndexDel(Idea& idea);
  // Private search methods to support string-only queries
  void runQueryOnBank(string query, unordered_set<int>& results);
  void runQueryOnIndex(string query, unordered_set<int>& results);
  void extractWordsFromQuery(string query, string& word1, string& word2);
  void set_union(unordered_set<int>& result, unordered_set<int>& a, unordered_set<int>& b);
  void set_intersect(unordered_set<int>& result, unordered_set<int>& a, unordered_set<int>& b);
  void showSearchResults(unordered_set<int>& idSet);

  public:
    // Insert idea from object
    void addIdea(Idea& newIdea);
    // Insert idea from keyboard
    // (uses the default constructor of the Idea object)
    void addIdea() {Idea newIdea; addIdea(newIdea);};
    void addIdeasFromFile(string fname);

    bool deleteIdea(int id);

    // Public searching methods which support AND/OR queries
    void searchFromBank(string query);
    void searchFromIndex(string query);
    void showIdea(int id);
    void showDatabase();
    void showIndex();
};

// Functions to be used as pointers
void printWordIndex(wordIndex data);
void indexEditAdd(wordIndex* data, string args[]);
void indexEditDel(wordIndex* data, string args[]);


#endif
