#include <iostream>
#include <string>
#include <vector>
#include <unordered_set>
#include <climits>
#include <cassert>

#include "constants.h"


#ifndef IDEA_H
#define IDEA_H


using namespace std;


// External find function to be used by Idea and other classes
int find(string kwd, string content);


class Idea {
  // Static set of all ids ever created to ensure 100% unique
  static unordered_set<int> ids;
  // An iterative measure to choose an ID, iterates as more IDs are created
  static int nId;
  int id;
  string proposer;
  vector<string> keywords;
  string content;

  void deSerialise(string serial, bool cpy);
  string sanitise(string str);
  int getNewId(int);

  public:
    // Constructor where variables are supplied
    Idea(string p, vector<string>* k, string c);
    // Copy constructor
    Idea(Idea* otherIdea) {deSerialise(otherIdea->serialise(), true);};
    // Constructor where a serialised string is supplied
    Idea(string serial) {deSerialise(serial, false);};
    // Constructor where user input is required
    Idea();

    int getId() {return id;};
    string getProposer() {return proposer;};
    void getAllWordsInIdea(unordered_set<string>& words);
    string serialise();
    void show();

    bool isKeyword(string kwd);
    bool isInContent(string kwd);
};


#endif
