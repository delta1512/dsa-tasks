
#ifndef CONSTANTS_H
#define CONSTANTS_H


using namespace std;


/*
FORMAT OF A SERIALISED IDEA:
1. Starts with START_IDEA_DELIM
2. Ends with END_IDEA_DELIM
3. For each part of the idea, wrap in DELIMITER unless it is
   next to a start or ending delimiter as in 1 and 2.
4. The order of members is as such: ID, proposer, keywords, content
5. keywords are comma-separated

Example:
<START_IDEA>1`My name`my,keyword,list`My idea content<END_IDEA>
*/


const string START_IDEA_DELIM = "<START_IDEA>";
const char DELIMITER = '`'; // Chosen as something that will likely not be used in writing
const char ALT_DELIM = '"'; // An alternate character to replace during sanitisation
const string END_IDEA_DELIM = "<END_IDEA>";


#endif
