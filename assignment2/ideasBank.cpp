/*********** Declaration*******
I hereby certify that no part of this assignment has been copied from
any other student’s work or from any other source. No part of the code
has been written/produced for me by another person or copied from any
other source.

I hold a copy of this assignment that I can produce if the original is
lost or damaged.
**************************/

#include "ideasBank.h"


// Re-indexes the index whenever a new idea is added
// Iterates through all the words in an idea and makes or appends indices
void IdeasBank::reIndexAdd(Idea& idea) {
  // Load the argument to be passed to the modify function
  string arguments[] = {to_string(idea.getId())};
  unordered_set<string> allWords;
  idea.getAllWordsInIdea(allWords);
  // For every word in all possible words of the idea, add it to the index
  for (unordered_set<string>::iterator i = allWords.begin(); i != allWords.end(); i++) {
    string key = *i;
    // If the modification was unsuccessful, then a new node must be created
    // Else nothing else to do
    if (!index.AVL_modify(key, indexEditAdd, arguments)) {
      wordIndex newNode;
      newNode.key = key;
      newNode.ids.push_back(idea.getId());
      index.AVL_Insert(newNode);
    }
  }
}


// Re-indexes the index whenever an idea is deleted
// Iterates through all the words of the idea and removes any instance of the
// idea ID from the indices.
void IdeasBank::reIndexDel(Idea& idea) {
  // Load the argument to be passed to the modify function
  string arguments[] = {to_string(idea.getId())};
  unordered_set<string> allWords;
  idea.getAllWordsInIdea(allWords);
  // For every word in all possible words of the idea, delete it from the index
  for (unordered_set<string>::iterator i = allWords.begin(); i != allWords.end(); i++) {
    string key = *i;
    // Modify without any further action
    index.AVL_modify(key, indexEditDel, arguments);
  }
}


// Add an idea to the ideas bank
void IdeasBank::addIdea(Idea& newIdea) {
  // Copy the new idea into the ideas bank
  pair<unordered_map<int,Idea>::iterator,bool> result = bank.insert(
    make_pair<int,Idea>(newIdea.getId(), Idea(&newIdea))
  );

  // Re-index if the insertion was successful
  // (an unsuccessful insertion means the idea already exists)
  if (result.second) {
    reIndexAdd(newIdea);
  }
}


// Add ideas to the bank from a file containing idea serialisations on each line
// Recommended to perform this first before adding new ideas as the IDs may be permanently changed
void IdeasBank::addIdeasFromFile(string fname) {
  ifstream input;
  input.open(fname);
  if (!input.good()) {
    cout << "Failed to open file: " << fname << endl;
    return;
  }

  string line;
  while (getline(input, line)) {
    Idea newIdea = Idea(line);
    addIdea(newIdea); // Use the serialisation copy constructor
  }
  input.close();

  cout << "Successfully read the file!" << endl;
}


// Delete an idea from the bank and index
bool IdeasBank::deleteIdea(int id) {
  // If the idea actually exists
  if (bank.count(id)) {
    Idea ideaToErase = bank.find(id)->second;
    // Delete from the index
    reIndexDel(ideaToErase);
    // Erase from the database
    bank.erase(id);
    return true;
  } else {
    return false;
  }
}


// Prints an idea given its id
void IdeasBank::showIdea(int id) {
  unordered_map<int,Idea>::iterator result = bank.find(id);
  if (result != bank.end()) {
    result->second.show();
  } else {
    cout << "Could not find idea with ID: " << id << endl;
  }
}


// Prints all the ideas in the database
void IdeasBank::showDatabase() {
  for (unordered_map<int,Idea>::iterator i = bank.begin(); i != bank.end(); i++) {
    cout << endl;
    i->second.show();
    cout << endl;
  }
}


// Shows what the AVL tree index looks like
void IdeasBank::showIndex() {
  index.AVL_Traverse(printWordIndex);
}


// Looks for a single word query in all the words of each idea in the bank
// Does not support boolean queries
// Results will be returned via the results set
void IdeasBank::runQueryOnBank(string query, unordered_set<int>& results) {
  for (unordered_map<int,Idea>::iterator i = bank.begin(); i != bank.end(); i++) {
    // If the query is found in the keywords or content
    if (i->second.isKeyword(query) || i->second.isInContent(query)) {
      results.insert(i->second.getId());
    }
  }
}


// Looks for a single word query in all the indices of the index
// Does not support boolean queries
// Results will be returned via the results set
void IdeasBank::runQueryOnIndex(string query, unordered_set<int>& results) {
  wordIndex foundIndex;
  // If the index is found
  if (index.AVL_Retrieve(query, foundIndex)) {
    // Iterate through the id list and insert the ideas
    for (list<int>::iterator i = foundIndex.ids.begin(); i != foundIndex.ids.end(); i++) {
      results.insert(*i);
    }
  }
}


// Extracts the single words from a normal or boolean query
// In a single word query, the word will be placed in word1
// In a boolean query, the left side will be placed in word1 and the right side, word2
// Limited to single operator queries
void IdeasBank::extractWordsFromQuery(string query, string& word1, string& word2) {
  word1 = "";
  // Find the first word
  char current = query[0];
  int seek = 0;
  while (current != ' ' && seek < query.size()) {
    word1 += current;
    current = query[++seek];
  }

  // Skip past the operator
  current = query[++seek];
  while (current != ' ' && seek < query.size()) {
    current = query[++seek];
  }

  word2 = "";
  // Find the second word
  current = query[++seek];
  while (current != ' ' && seek < query.size()) {
    word2 += current;
    current = query[++seek];
  }
}


// Performs the union of two sets and returns the results in the result set
void IdeasBank::set_union(unordered_set<int>& result, unordered_set<int>& a, unordered_set<int>& b) {
  // Insert everything in set a into the results
  for (unordered_set<int>::iterator i = a.begin(); i != a.end(); i++) {
    result.insert(*i);
  }
  // Insert everything in set b into the results
  // Need not worry about duplicates because of the hashmap
  for (unordered_set<int>::iterator i = b.begin(); i != b.end(); i++) {
    result.insert(*i);
  }
}


// Performs the intersection of two sets and returns the results in the result set
void IdeasBank::set_intersect(unordered_set<int>& result, unordered_set<int>& a, unordered_set<int>& b) {
  for (unordered_set<int>::iterator i = a.begin(); i != a.end(); i++) {
    // If the element of a is in b the nadd it to the results
    if (b.find(*i) != b.end()) {
      result.insert(*i);
    }
  }
}


// Prints a set of ideas given their ids
void IdeasBank::showSearchResults(unordered_set<int>& idSet) {
  cout << "Here are the Idea-ids from the search results..." << endl;
  for (unordered_set<int>::iterator i = idSet.begin(); i != idSet.end(); i++) {
    cout << *i << endl;
  }
}


// Public search function for users to use that searches the bank
// Takes a standard or boolean query with a maximum of 2 terms
void IdeasBank::searchFromBank(string query) {
  if (query.size() == 0) {
    cout << "No word was provided" << endl;
    return;
  }

  cout << "Searching from the bank..." << endl;
  // Initialise the result sets
  unordered_set<int> results, word1, word2;
  string word1str, word2str;

  extractWordsFromQuery(query, word1str, word2str);
  // There will always be at least 1 search term, process it
  runQueryOnBank(word1str, word1);
  // If it is a boolean query, perform the query using the other term
  if (word2str.size()) {
    runQueryOnBank(word2str, word2);
  }

  // Decide whether or not it is an AND or OR query and handle accordingly
  // For standard queries, a union with the empty set is valid
  if (query.find("AND") != query.npos) {
    set_intersect(results, word1, word2);
  } else {
    set_union(results, word1, word2);
  }

  showSearchResults(results);
}


// Public search function for users to use that searches the index
// Takes a standard or boolean query with a maximum of 2 terms
void IdeasBank::searchFromIndex(string query) {
  if (query.size() == 0) {
    cout << "No word was provided" << endl;
    return;
  }

  cout << "Searching from the index..." << endl;
  // Initialise the result sets
  unordered_set<int> results, word1, word2;
  string word1str, word2str;

  extractWordsFromQuery(query, word1str, word2str);
  // There will always be at least 1 search term, process it
  runQueryOnIndex(word1str, word1);
  // If it is a boolean query, perform the query using the other term
  if (word2str.size()) {
    runQueryOnIndex(word2str, word2);
  }

  // Decide whether or not it is an AND or OR query and handle accordingly
  // For standard queries, a union with the empty set is valid
  if (query.find("AND") != query.npos) {
    set_intersect(results, word1, word2);
  } else {
    set_union(results, word1, word2);
  }

  showSearchResults(results);
}


// External functions for the ideasBank
// These functions are external because they must be turned into pointers
// and passed to other functions. To avoid issues, these are kept external.

// Used for the AVL tree traversal to print each index item
void printWordIndex(wordIndex data) {
  cout << "Index of word: " << data.key << endl;
  cout << "ID list: ";
  for (list<int>::iterator i = data.ids.begin(); i != data.ids.end(); i++) {
    cout << *i << ", ";
  }
  cout << endl;
}


// Edits an index by adding a new id to the id list
void indexEditAdd(wordIndex* data, string args[]) {
  data->ids.push_back(stoi(args[0]));
}


// Edits an index by deleting the provided id
void indexEditDel(wordIndex* data, string args[]) {
  int term = stoi(args[0]);
  // Delete from the id list only if it is in there
  for (list<int>::iterator x = data->ids.begin(); x != data->ids.end(); x++) {
    if ((*x) == term) {
      data->ids.erase(x);
      break;
    }
  }
}
