/*********** Declaration*******
I hereby certify that no part of this assignment has been copied from
any other student’s work or from any other source. No part of the code
has been written/produced for me by another person or copied from any
other source.

I hold a copy of this assignment that I can produce if the original is
lost or damaged.
**************************/

#include <iostream>
#include <unordered_set>

#include "idea.h"
#include "ideasBank.h"


using namespace std;


int main() {
  // Initialise the bank
  IdeasBank mainBank;
  int choice;

  cout << "Welcome to the Ideas Bank by Marcus Belcastro (19185398)" << endl << endl;

  do {
    cout << "1. Add an idea" << endl;
    cout << "2. Show idea" << endl;
    cout << "3. Delete an idea" << endl;
    cout << "4. Show the database" << endl;
    cout << "5. Show the index" << endl;
    cout << "6. Search via the bank" << endl;
    cout << "7. Search via the index" << endl;
    cout << "8. Read a file" << endl;
    cout << "9. Exit" << endl;
    cin >> choice;

    cout << endl;
    // Solve the bug with cin skipping inputs
    cin.ignore(10000, '\n');
    // Avoid initialising in the switch statement
    string search, path;
    int id;
    switch (choice) {
      case 1:
        mainBank.addIdea();
        break;
      case 2:
        cout << "ID to show: ";
        cin >> id;
        mainBank.showIdea(id);
        break;
      case 3:
        cout << "ID to delete: ";
        cin >> id;
        mainBank.deleteIdea(id);
        break;
      case 4:
        mainBank.showDatabase();
        break;
      case 5:
        mainBank.showIndex();
        break;
      case 6:
        cout << "Type your search query..." << endl;
        getline(cin, search);
        mainBank.searchFromBank(search);
        break;
      case 7:
        cout << "Type your search query..." << endl;
        getline(cin, search);
        mainBank.searchFromIndex(search);
        break;
      case 8:
        cout << "Type The path of the file to read..." << endl;
        getline(cin, path);
        mainBank.addIdeasFromFile(path);
        break;
      case 9:
        cout << "Thanks for using the Ideas Bank!" << endl;
        break;
    }
    cout << endl;
  } while (choice > 0 && choice < 9);

  return 0;
}
