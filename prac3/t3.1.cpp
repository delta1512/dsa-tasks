#include <iostream>
#include <cstdlib>
#include <ctime>
#include <list>

using namespace std;

class State {
  list<int> stateArray = list<int>(9, 0);

  public:
    State() {};
    int operator[](int x) {
      list<int>::iterator iter = stateArray.begin();
      for (int i = 0; i < x; i++) {iter++;}
      return *iter;
    };
    void set(int index, int val) {
      list<int>::iterator iter = stateArray.begin();
      for (int i = 0; i < index; i++) {iter++;}
      *iter = val;
    };
};

int getRand(int a, int b, int& nRand);
void chooseRandNums(State& numList, int& nRand);


// constant string to act as a separator
const string sep = " --- --- --- \n";


int main() {
  // Seed the random number generator
  srand(time(NULL));

  // Initialise a vector of int lists (int ptrs)
  list<State> numbers;
  // nRand is how many random calls have been made
  int nRand=0, current;


  // Initialise the vector of numbers
  for (int i = 0; i < 10; i++) {
    State currentNums;
    numbers.push_back(currentNums);
  }

  // For every list of numbers
  for (list<State>::iterator i = numbers.begin(); i != numbers.end(); i++) {
    // Choose the numbers for the array
    chooseRandNums(*i, nRand);

    // Print the array as a grid
    cout << sep;
    for (int j = 0; j < 3; j++) {
      for (int k = 0; k < 3; k++) {
        cout << "| " << (*i)[(j * 3) + k] << " ";
      }
      cout << "|" << endl;
    }
    cout << sep;
    // Reset the random count to 0
    nRand = 0;
  }

  return 0;
}


int getRand(int a, int b, int& nRand) {
  // nRand counts the number of random numbers have been generated
  nRand++;
  return rand() % (b-a+1) + a;
}


void chooseRandNums(State& numList, int& nRand) {
  // Ensure the list is initialised to all-zeros
  for (int i = 0; i < 9; i++) {
    numList.set(i, 0);
  }

  // Get a random number to start with
  int current = getRand(0, 8, nRand);
  for (int i = 1; i <= 6; i++) {
    // Keep getting random numbers until a free space is found at that index
    while (numList[current] != 0) {
      current = getRand(0, 8, nRand);
    }
    // Set the current index to the iteration of the for loop
    numList.set(current, i);
  }
}
