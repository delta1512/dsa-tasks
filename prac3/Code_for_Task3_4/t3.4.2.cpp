#include <iostream>
#include <cstdlib>
#include <ctime>

#include "unorderedLinkedList.h"
#include "state.h"

using namespace std;

// constant string to act as a separator
const string sep = " --- --- --- \n";
const int NUMLISTS = 10000;
const int searchTermArr[9] = {2, 1, 5, 4, 0, 0, 6, 0, 3};


int main() {
  State searchTerm = State();
  // Seed the random number generator
  //long int seed = 1585034974l; // Working seed for the RNG
  long int seed = time(NULL);
  srand(seed);
  cout << "Here's the current time seed: " << seed << endl << endl;

  // Load the state with the search term array
  for (int i = 0; i < 9; i++) {
    searchTerm.set(i, searchTermArr[i]);
  }

  // Initialise a vector of int lists (int ptrs)
  unorderedLinkedList<State> numbers;
  // nRand is how many random calls have been made
  int nRand=0, current;
  State currentNums;

  // Initialise the vector of numbers
  for (int i = 0; i < NUMLISTS; i++) {
    currentNums = State();
    currentNums.chooseRandNums();
    numbers.insertFirst(currentNums);
  }

  if (numbers.search(searchTerm)) {
    cout << "We found it!" << endl;
  } else {
    cout << "Did not find anything :(" << endl;
  }

  return 0;
}


int getRand(int a, int b, int& nRand) {
  // nRand counts the number of random numbers have been generated
  nRand++;
  return rand() % (b-a+1) + a;
}


void chooseRandNums(State& numList, int& nRand) {
  // Ensure the list is initialised to all-zeros
  for (int i = 0; i < 9; i++) {
    numList.set(i, 0);
  }

  // Get a random number to start with
  int current = getRand(0, 8, nRand);
  for (int i = 1; i <= 6; i++) {
    // Keep getting random numbers until a free space is found at that index
    while (numList[current] != 0) {
      current = getRand(0, 8, nRand);
    }
    // Set the current index to the iteration of the for loop
    numList.set(current, i);
  }
}
