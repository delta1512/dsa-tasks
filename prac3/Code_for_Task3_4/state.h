#include "orderedLinkedList.h"

using namespace std;

class State {
  orderedLinkedList<int> stateArray = orderedLinkedList<int>();

  public:
    // Initialise as all zeroes
    State() {
      int* test;
      for (int i = 0; i < 9; i++) {
        test = new int(0);
        stateArray.insertFirst(*test);
      }
    };
    int operator[](int k) {
      return stateArray.getKThElement(k);
    };

    bool operator==(State x) {
      for (int i = 0; i < 9; i++) {
        if (x[i] != stateArray.getKThElement(i)) {
          return false;
        }
      }
      return true;
    };

    void set(int index, int val) {
      linkedListIterator<int> iter = stateArray.begin();
      for (int i = 0; i < index; i++) {iter++;}
      iter = val;
    };


    void show() {
      linkedListIterator<int> iter = stateArray.begin();
      for (int i = 0; i < 9; i++) {
        cout << *iter << " ";
        iter++;
      }
      cout << endl;
    }


    int getRand(int a, int b) {
      return rand() % (b-a+1) + a;
    }


    void chooseRandNums() {
      // Get a random number to start with
      int current = getRand(0, 8);
      for (int i = 1; i <= 6; i++) {
        // Keep getting random numbers until a free space is found at that index
        while (stateArray.getKThElement(current) != 0) {
          current = getRand(0, 8);
        }
        // Set the current index to the iteration of the for loop
        set(current, i);
      }
    }
};
