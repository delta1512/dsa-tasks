#include <iostream>
#include <cstdlib>
#include <ctime>

#include "orderedLinkedList.h"
#include "state.h"

using namespace std;

// constant string to act as a separator
const string sep = " --- --- --- \n";


int main() {
  // Seed the random number generator
  srand(time(NULL));

  // Initialise a vector of int lists (int ptrs)
  orderedLinkedList<State> numbers;
  // nRand is how many random calls have been made
  int nRand=0, current;
  State currentNums;

  // Initialise the vector of numbers
  for (int i = 0; i < 10; i++) {
    currentNums = State();
    currentNums.chooseRandNums();
    numbers.insertFirst(currentNums);
  }

  // For every list of numbers
  for (int i = 0; i < 10; i++) {
    State currentState = numbers.getKThElement(i);

    // Print the array as a grid
    cout << sep;
    for (int j = 0; j < 3; j++) {
      for (int k = 0; k < 3; k++) {
        cout << "| " << (currentState)[(j * 3) + k] << " ";
      }
      cout << "|" << endl;
    }
    cout << sep;
    // Reset the random count to 0
    nRand = 0;
  }

  return 0;
}
