/*
 * state.h
 *
 *  Created on: 29/02/2020
 *      Author: dongmo
 */

#include "linkedStack.h"


#ifndef BOARD_H_
#define BOARD_H_

const int BOARDSIZE = 3;
const int NUMBER_OF_BLOCKS = 6;

class State {
private:
	int grid[BOARDSIZE][BOARDSIZE];
public:
	State() {
		for (int i = 0; i < BOARDSIZE; i++)
			for (int j = 0; j < BOARDSIZE; j++)
				grid[i][j] = rand() % 7;
	}

	//For Task 3.4
	bool operator==(State s) {
		for (int i = 0; i < BOARDSIZE; i++)
			for (int j = 0; j < BOARDSIZE; j++)
				if (s.grid[i][j] != grid[i][j])
					return false;
		return true;
	}

	//For Task 3.4
	bool operator!=(State s) {
		for (int i = 0; i < BOARDSIZE; i++)
			for (int j = 0; j < BOARDSIZE; j++)
				if (s.grid[i][j] != grid[i][j])
					return true;
		return false;
	}

	void printBoard();
	void pushDown();
};

void State::printBoard() {
	cout << endl;

	for (int i = 0; i < BOARDSIZE; i++) {
		for (int j = 0; j < BOARDSIZE; j++) {
			cout << " " << grid[BOARDSIZE - i - 1][j] << " ";
		}
		cout << endl;
	}
}

void State::pushDown() {
	// Iterate through the columns one-by-one
	for (int col = 0; col < BOARDSIZE; col++) {
		// Initiate the pusher to push numbers down
		linkedStackType<int> pusher;
		for (int row = BOARDSIZE-1; row >= 0; row--) {
			// If a non-zero is found
			if (grid[row][col] != 0) {
				// Push it to the pusher
				pusher.push(grid[row][col]);
			}
			// zero-out the column
			grid[row][col] = 0;
		}
		for (int i = 0; !pusher.isEmptyStack(); i++) {
			// place them back into the array
			grid[i][col] = pusher.top();
			// get rid of the top item and move on
			pusher.pop();
		}
	}
}

#endif /* BOARD_H_ */
